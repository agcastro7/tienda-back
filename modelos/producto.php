<?php 
class Producto {
    // conexion con la bd y nombre de la tabla
   private $conexion;
   private $tabla = "productos";

   // columnas
   public $id;
   public $nombre;
   public $tipo;
   public $subtipo;
   public $precio;
   public $descuento;
   public $descripcion;
   public $imagen;

    // constructor
    public function __construct($conexion){
        $this->conexion = $conexion;
    }

    // Devuelve todos los productos de la base de datos
    public function todos(){
        $consulta = "SELECT * FROM ".$this->tabla;
        $resultado = $this->conexion->query($consulta);
        return $resultado;
    }

    // copiar los datos de otro producto
    public function copiar($otro){
        $this->id = $otro->id;
        $this->nombre = $otro->nombre;
        $this->tipo = $otro->tipo;
        $this->subtipo = $otro->subtipo;
        $this->descripcion = $otro->descripcion;
        $this->precio = $otro->precio;
        $this->descuento = $otro->descuento;
        $this->imagen = $otro->imagen;
    }

    // insertar un nuevo producto
    public function insertar(){
        $consulta = "INSERT INTO ".$this->tabla."(nombre, tipo, subtipo, descripcion, precio, descuento, imagen) VALUES ('".$this->nombre."', '".$this->tipo."', '".$this->subtipo."', '".$this->descripcion."', ".$this->precio.", ".$this->descuento.", '".$this->imagen."');";
        $resultado = $this->conexion->query($consulta);
        return $resultado;
    }
}
?>