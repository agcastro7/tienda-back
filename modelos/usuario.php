<?php
class Usuario{
   // conexion con la bd y nombre de la tabla
   private $conexion;
   private $tabla = "usuarios";
 
   // columnas
   public $id;
   public $nombre;
   public $apellidos;
   public $correo_electronico;
   public $direccion;
   public $telefono;
   public $contrasena;
   public $admin;
 
   // constructor
   public function __construct($conexion){
       $this->conexion = $conexion;
   }

    // buscar un usuario por su correo electrónico
    public function buscarPorCorreo($correo){
        $consulta = "SELECT * FROM ".$this->tabla." WHERE correo_electronico='".$correo."'";
        $resultado = $this->conexion->query($consulta);
        if($resultado->num_rows == 0){
            return 0;
        }
        else{
            $fila = $resultado->fetch_assoc();
            $this->id = $fila["id"];
            $this->nombre = $fila["nombre"];
            $this->apellidos = $fila["apellidos"];
            $this->correo_electronico = $fila["correo_electronico"];
            $this->direccion = $fila["direccion"];
            $this->telefono = $fila["telefono"];
            $this->contrasena = $fila["contrasena"];
            $this->admin = $fila["admin"];

            return $this->id;
        }
    }

    public function buscarPorId($id){
        $consulta = "SELECT * FROM ".$this->tabla." WHERE id='".$id."'";
        $resultado = $this->conexion->query($consulta);
        if($resultado->num_rows == 0){
            return 0;
        }
        else{
            $fila = $resultado->fetch_assoc();
            $this->id = $fila["id"];
            $this->nombre = $fila["nombre"];
            $this->apellidos = $fila["apellidos"];
            $this->correo_electronico = $fila["correo_electronico"];
            $this->direccion = $fila["direccion"];
            $this->telefono = $fila["telefono"];
            $this->contrasena = $fila["contrasena"];
            $this->admin = $fila["admin"];

            return $this->id;
        }
    }


    // leer todos los usuarios
    public function todos(){
        $consulta = "SELECT * FROM ".$this->tabla;
        $resultado = $this->conexion->query($consulta);
        return $resultado;
    }

    // insertar un nuevo usuario
    public function insertar(){
        $consulta = "INSERT INTO ".$this->tabla."(nombre, apellidos, correo_electronico, direccion, telefono, contrasena) VALUES ('".$this->nombre."', '".$this->apellidos."', '".$this->correo_electronico."', '".$this->direccion."', '".$this->telefono."', '".$this->contrasena."');";
        $resultado = $this->conexion->query($consulta);
        return $resultado;
    }

    // copiar los datos de otro usuario
    public function copiar($otro){
        $this->id = $otro->id;
        $this->nombre = $otro->nombre;
        $this->apellidos = $otro->apellidos;
        $this->correo_electronico = $otro->correo_electronico;
        $this->direccion = $otro->direccion;
        $this->telefono = $otro->telefono;
        $this->contrasena = $otro->contrasena;
        $this->admin = $otro->admin;
    }

    
 
}
