<?php
// cabeceras
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// incluir base de datos y modelo
include_once '../config/basedatos.php';
include_once '../modelos/producto.php';

// conexion con base de datos
$bd = new BaseDatos();
$conexion = $bd->getConexion();
$producto = new Producto($conexion);

// leer productos
$resultado = $producto->todos();
$num = $resultado->num_rows;

// Si el numero de productos es mayor que cero, continuamos
if($num>0){
    // creamos un array que usaremos como respuesta
    $productos_arr=array();
    $productos_arr["filas"]=array();

    while($fila = $resultado->fetch_assoc()){
        $producto_fila = array(
            "id" => $fila["id"],
            "nombre" => $fila["nombre"],
            "tipo" => $fila["tipo"],
            "subtipo" => $fila["subtipo"],
            "descripcion" => $fila["descripcion"],
            "precio" => $fila["precio"],
            "descuento" => $fila["descuento"],
            "imagen" => $fila["imagen"]
        );
        array_push($productos_arr["filas"], $producto_fila);
    }

    // codigo respuesta http - 200 OK
    http_response_code(200);
    
    // codificamos los datos en json
    echo json_encode($productos_arr);
}
// Si no, mostramos error
else{
    // codigo respuesta http - 404 Not found
    http_response_code(404);
    // mensaje de error
    echo json_encode(
        array("mensaje" => "No se encontraron productos.")
    );
}

$conexion->close();
?>