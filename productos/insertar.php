<?php 
// cabeceras
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// incluir base de datos y modelo
include_once '../config/basedatos.php';
include_once '../modelos/producto.php';
 
// conexion con base de datos
$bd = new BaseDatos();
$conexion = $bd->getConexion();
$producto = new Producto($conexion);

// recibimos los datos del producto mediante POST
$datos_post = file_get_contents("php://input");
$peticion = json_decode($datos_post);

if(isset($peticion->producto)){
    // marcamos el id a -1 porque aun no esta insertado, y lo copiamos
    $peticion->producto->id = -1;
    $producto->copiar($peticion->producto);

    if($producto->insertar()){
        $producto->id = $conexion->insert_id;

        $producto_arr = array();
        $producto_arr["producto"] = array($producto);

        // codigo respuesta http - 200 OK
        http_response_code(200);
        
        // codificamos los datos en json
        echo json_encode($producto_arr);
    }
    else{
        // codigo de respuesta http - 503 service unavailable
        http_response_code(503);
    
        // mensaje de error
        echo json_encode(array("mensaje" => "No se pudo crear el producto (error interno)"));
    }
}
else{
    // codigo de respuesta http - 400 bad request
    http_response_code(400);
 
    // mensaje de error
    echo json_encode(array("mensaje" => "Los datos recibidos para el producto estan incompletos"));
}


$conexion->close();

?>