<?php
// cabeceras
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// incluir base de datos y modelo
include_once '../config/basedatos.php';
include_once '../modelos/usuario.php';

// conexion con base de datos
$bd = new BaseDatos();
$conexion = $bd->getConexion();
$usuario = new Usuario($conexion);

// tomar los datos enviados
$datos_get = file_get_contents("php://input");
$peticion = json_decode($datos_get);

if(isset($peticion->login)){
  if($usuario->buscarPorCorreo($peticion->login->correo_electronico)){
    if($usuario->contrasena == $peticion->login->contrasena){
      // codigo respuesta http - 200 OK
      http_response_code(200);

      // codificamos los datos en json
      $usuario_arr = array();
      $usuario_arr["usuario"] = $usuario;
      echo json_encode($usuario_arr);
    }
    else{
      // codigo de respuesta http - 403 forbidden
      http_response_code(403);

      // mensaje de error
      echo json_encode(array("mensaje" => "La contraseña indicada no es correcta"));
    }
  }
  else{
    // codigo de respuesta http - 404 not found
    http_response_code(404);

    // mensaje de error
    echo json_encode(array("mensaje" => "El correo electrónico indicado no pertenece a un usuario registrado"));
  }
}
else{
  // codigo de respuesta http - 400 bad request
  http_response_code(400);

  // mensaje de error
  echo json_encode(array("mensaje" => "Los datos recibidos para el usuario estan incompletos"));
}
?>
