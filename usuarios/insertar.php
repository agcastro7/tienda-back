<?php
// cabeceras
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// incluir base de datos y modelo
include_once '../config/basedatos.php';
include_once '../modelos/usuario.php';
 
// conexion con base de datos
$bd = new BaseDatos();
$conexion = $bd->getConexion();
$usuario = new Usuario($conexion);

// recibimos los datos del usuario mediante POST
$datos_post = file_get_contents("php://input");
$peticion = json_decode($datos_post);
 
if(isset($peticion->usuario)){
    if(!$usuario->buscarPorCorreo($peticion->usuario->correo_electronico)){
        // obtenemos las variables enviadas mediante POST
        $peticion->usuario->id = -1;
        $peticion->usuario->admin = 0;
        $usuario->copiar($peticion->usuario);

        // insertamos el usuario en la BD
        $resultado = $usuario->insertar();
        if($resultado){
            $usuario->id = $conexion->insert_id;
        
            $usuario_arr = array();
            $usuario_arr["usuario"] = array($usuario);
        
            // codigo respuesta http - 200 OK
            http_response_code(200);
        
            // codificamos los datos en json
            echo json_encode($usuario_arr);
        }
        else{
            // codigo de respuesta http - 503 service unavailable
            http_response_code(503);
    
            // mensaje de error
            echo json_encode(array("mensaje" => "No se pudo crear el usuario (error interno)"));
        }
    }
    else{
        // codigo de respuesta http - 409 conflict
        http_response_code(409);
 
        // mensaje de error
        echo json_encode(array("mensaje" => "El correo electrónico indicado ya pertenece a un usuario registrado"));
    }
}
else{
    // codigo de respuesta http - 400 bad request
    http_response_code(400);
 
    // mensaje de error
    echo json_encode(array("mensaje" => "Los datos recibidos para el usuario estan incompletos"));
}

$conexion->close();
 
?>
