<?php
// cabeceras
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// incluir base de datos y modelo
include_once '../config/basedatos.php';
include_once '../modelos/usuario.php';

// conexion con base de datos
$bd = new BaseDatos();
$conexion = $bd->getConexion();
$usuario = new Usuario($conexion);

// comprobamos que se han recibido los datos correctos
if(isset($_GET["id"])){
    $id = $_GET["id"];
    $usuario = new Usuario($conexion);
    if($usuario->buscarPorId($id)){
        // codigo respuesta http - 200 OK
        http_response_code(200);

        // codificamos los datos en json
        $usuario_arr = array();
        $usuario_arr["usuario"] = $usuario;
        echo json_encode($usuario_arr);
    }
    else{
        // codigo de respuesta http - 404 not found
    http_response_code(404);

    // mensaje de error
    echo json_encode(array("mensaje" => "El id indicado no pertenece a un usuario registrado"));
    }
}
else{
    // codigo de respuesta http - 400 bad request
  http_response_code(400);

  // mensaje de error
  echo json_encode(array("mensaje" => "Los datos recibidos para el usuario estan incompletos"));
}

 
$conexion->close();
?>