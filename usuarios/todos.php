<?php
// cabeceras
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// incluir base de datos y modelo
include_once '../config/basedatos.php';
include_once '../modelos/usuario.php';

// conexion con base de datos
$bd = new BaseDatos();
$conexion = $bd->getConexion();
$usuario = new Usuario($conexion);

// leer usuarios
$resultado = $usuario->todos();
$num = $resultado->num_rows;

if($num>0){
    
    // creamos un array que usaremos como respuesta
    $usuarios_arr=array();
    $usuarios_arr["filas"]=array();
 
    while($fila = $resultado->fetch_assoc()){
        $usuario_fila = array(
            "id" => $fila["id"],
            "nombre" => $fila["nombre"],
            "apellidos" => $fila["apellidos"],
            "correo_electronico" => $fila["correo_electronico"],
            "direccion" => $fila["direccion"],
            "telefono" => $fila["telefono"],
            "contrasena" => $fila["contrasena"],
            "admin" => $fila["admin"]
        );
        
        array_push($usuarios_arr["filas"], $usuario_fila);
    }

    // codigo respuesta http - 200 OK
    http_response_code(200);
    
    // codificamos los datos en json
    echo json_encode($usuarios_arr);

 }
 else{
    // codigo respuesta http - 404 Not found
    http_response_code(404);
    // mensaje de error
    echo json_encode(
        array("mensaje" => "No se encontraron usuarios.")
    );
 }
 
 
?>
